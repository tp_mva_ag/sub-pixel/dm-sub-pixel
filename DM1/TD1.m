%% Constantes
D = 50e-3;          % diam�tre pupille mm
lambda = 400e-9;    % Longueur d'onde consid�r�e : Violet
c = 3e8;            % Vitesse de la lumi�re
f = c / lambda;     % Fr�quence de la lumi�re consid�r�e
C = (pi^2 * D^4) / (32 * f^2);  % Constante de calcul 

%% Question 1

R = linspace(0.1,10,100);     % On fait varier R de 0.1 � 10
Kdiff = (2 * besselj(1,R) ./ R).^2;     % On calcule Kdiff (normalis�e)

figure(1); plot(R, Kdiff);     % On affiche Kdiff en fonction de R
grid ON; xlabel("R"); ylabel("K_d_i_f_f / C");
title("Intensit� de la t�che optique en fonction de la distance au centre.")

%% Question 2

xgv = linspace(-10,10,100); ygv = linspace(-10,10,100);
[X, Y] = meshgrid(xgv, ygv);    % On cr�e nos matrices d'�chantillonnage

R = sqrt(X.^2 + Y.^2);      % On calcule la valeur de la distance au centre pour chaque point
Kdiff = (2 * besselj(1,R) ./ R).^2;     % On d�termine la valeur de Kdiff pour cette matrice

figure(2); imshow(Kdiff, [0, 0.1]);    % On l'affiche 
title("Intensit� de la t�che optique, pour une ouverture de diam�tre 1mm")

%% Exo 3a

rho = linspace(0, 1, 100);  % On fait varier la variable rho de 0 � 1

Kfourier = (lambda^2 * D^2)/4 *(acos(rho) - rho .* sqrt(1 - rho.^2));
    % On d�termine la transform�e de Fourier � partir de la formule
    % analytique
K0 = Kfourier(1);
    % On obtient sa valeur en 0 pour le normaliser

figure(3); subplot(2, 1, 1); plot(rho, Kfourier/K0); % On affiche
ylabel("K_f_o_u_r_i_e_r normalis�e"); xlabel("\rho");
title("Transform�e de Fourier radiale de la t�che de diffraction");

% La TF est nulle au-dela de 1 : la diffraction �limine tous les d�tails
% au-del� d'une certaine fr�quence, et att�nue les autres.
% La diffraction fait du flou car elle att�nue les HF de l'image. Un flou
% particulier car un flou o� la TF a un support compact

xgv = linspace(-1,1,100); ygv = linspace(-1,1,100);
[X, Y] = meshgrid(xgv, ygv);    % On cr�e nos matrices d'�chantillonnage

rho = sqrt(X.^2 + Y.^2) ;    % Rho vaut la distance au centre encore
Kfourier = (lambda^2 * D^2)/4 *(acos(rho) - rho .* sqrt(1 - rho.^2));
    % On d�termine la transform�e de Fourier en 2D � partir de la formule
    % analytique

subplot(2, 2, 3); imshow(abs(Kfourier), [])   % On l'affiche
title("TF 2D de la t�che de diffraction")

%% Exo 3b

xgv = linspace(-100,100,100); ygv = linspace(-100,100,100);
[X, Y] = meshgrid(xgv, ygv);    % On cr�e nos matrices d'�chantillonnage
R = sqrt(X.^2 + Y.^2);  % On d�termine la distance au centre pour chaque pt

Kdiff = (2 * besselj(1,R) ./ R).^2;
    % On re-calcule Kdiff en 2D

% On d�termine la TF par calcul num�rique
Kfourier = fftshift(abs(fft2(Kdiff))); 

subplot(2, 2, 4); imshow(Kfourier,[]) % On l'affiche
title("TF 2D num�rique (FFT) de la t�che de diffraction")

%% Exo 4
figure(5)
% Nous allons faire une boucle dans laquelle nous additionneront 
% deux t�ches de diffraction, l�g�rement d�cal�es l'une par rapport 
% � l'autre. 
% Ce d�calage augmentera � chaque tour de boucle. Nous regarderons 
% alors o� se situe le maximum de cette somme : tant que les deux t�ches 
% seront proches, il sera entre les deux centre des t�ches, et les 
% deux t�ches seront indiscernables (cf. Fig 8). Quand elles 
% s'�loigneront suffisamment, leurs deux maximums seront visibles sur 
% la somme (cf. Fig 9), elles seront alors discernables. Ce moment 
% correspondra aussi au moment o� le maximum de la fonction ne sera plus 
% entre les deux centres, mais au niveau d'un d'eux.
% Ainsi, � chaque tour de boucle, nous calculeront la position du maximum
% de la somme : si il se trouve en 0 ou en epsilon (l'�cart entre les deux
% centres), et non entre les deux, c'est que les t�ches seront
% discernables. La premi�re valeur d'epsilon pour laquelle cela arricera
% sera donc la r�solution de notre appareil : la distance minimale entre
% deux objets �pour qu'ils puissent �tre distingu�s.

R = linspace(0,10,1000); % On �chantillonne R entre 0 et 10
epsilonListe = [];  % La liste des epsilons test�s
positionMax= [];     % On stocke la position du maximum
Kdiff = (2 * besselj(1,R) ./ R).^2;

for i = 1:numel(R)/2
    epsilon = R(i);
    R2 = R - epsilon;
    Kdiff2 = (2 * besselj(1,R2) ./ R2).^2;
    
    Ksomme = Kdiff + Kdiff2;
    
    [~, indice] = max(Ksomme);
    % On ajoute epsilon et la position du maximum � la liste
    epsilonListe = [epsilonListe epsilon];  
    positionMax = [positionMax R(indice)];
    
    % Cas non-diff�rentiable
    if(epsilon <= 1 && epsilon > 0.9)
        subplot(2,3,1); plot(R, abs(Ksomme))
        title("Exemple o� les deux t�ches sont non-dif�rentiables")
    end
    % Cas juste diff�rencitable
    if(epsilon <= 3.2 && epsilon >3.1)
        subplot(2,3,2); plot(R, Ksomme)
        title("Limite de dif�rentiabilit�")
    end
    % Cas tr�s diff�rentiable
    if(epsilon <= 5 && epsilon >4.9)
        subplot(2,3,3); plot(R, Ksomme)
        title("Exemple o� les deux t�ches sont bien dif�rentiables")
    end
end

subplot(2,1,2)
plot(epsilonListe, positionMax, 'bx', epsilonListe, epsilonListe,...
    'r--', epsilonListe, epsilonListe/2, 'g--');

xlabel("\epsilon_r : �cart entre les deux centres")
legend("Positions du maximum de la somme", "\epsilon_r = \epsilon_r",...
    "\epsilon_r = \epsilon_r/2 : maximum entre les 2 t�ches")
title("R�sultat de la d�termination de la distance minimale entre deux t�ches pour qu'elles soient discernables")

%% Question 5
try close 6; catch; end; figure(6)

R = linspace(0.1,10,100);       % On fait varier R de 0.1 � 10
epsilon = [0.25, 0.5, 0.75];    % Valeurs test�es

for i=1:3
    eps = epsilon(i);
    Kdiff = (2 * (besselj(1,R) - eps * besselj(1, eps * R)) ./ R).^2;     
        % On calcule Kdiff (normalis�e)
    
    hold; plot(R, Kdiff);     % On affiche Kdiff en fonction de R

    xlabel("R"); ylabel("K_d_i_f_f / C (normalis�e)");
    title("K_d_i_f_f(\epsilon) normalis� pour diff�rentes valeurs de \epsilon")
    hold;
end

legend("\epsilon = " + epsilon(1),...
    "\epsilon = " + epsilon(2), "\epsilon = " + epsilon(3))

%% Question 6
% Cas avec un diaphragme simple

lim = 400;
xgv = linspace(-lim,lim,500); ygv = linspace(-lim,lim,500);
[X, Y] = meshgrid(xgv, ygv);    % On cr�e nos matrices d'�chantillonnage

R = sqrt(X.^2 + Y.^2);  % On d�termine la distance au centre pour chaque pt
Kdiff = (2 * besselj(1,R) ./ R).^2;
    % On re-calcule Kdiff en 2D

% On d�termine la TF par calcul num�rique
Kfourier = fftshift(abs(fft2(Kdiff)));

figure(7); subplot(1,2,1); imshow(Kfourier,[]) % On l'affiche
title("Transform�e de Fourier 2D pour un diaphragme classique")

% Pour le cas avec le second disque
subplot(1,2,2)

eps = 1/4;
xgv = linspace(-lim,lim,500);
ygv = linspace(-lim,lim,500);
[X, Y] = meshgrid(xgv, ygv);    % On cr�e nos matrices d'�chantillonnage

R = sqrt(X.^2 + Y.^2);  % On d�termine la distance au centre pour chaque point
Kdiff2 = (2 * (besselj(1,R) - eps * besselj(1, eps * R)) ./ R).^2;
    % On re-calcule Kdiff en 2D

Kfourier2 = fft2(Kdiff2); % On d�termine la TF par calcul num�rique
Kfourier2 = fftshift(abs(Kfourier2)); % On la recale et en prend le module
imshow(Kfourier2,[]) % On l'affiche

title("Transform�e de Fourier 2D pour une pupille de t�l�scope")

