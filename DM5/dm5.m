%% Exercice 16

% Image et sa d�cal�e
u = double(imread('room.pgm'));
v = ffttrans(u,0.5,0.5);

% On les affiche
figure(1); subplot(1,2,1); imshow(u,[0,255]); title('Image originale')
subplot(1,2,2); imshow(v,[0,255]); title('Image d�cal�e de 1/2')

% On affiche la diff�rence pour mettre en avant le ringing
figure(2)
imshow(u-v, []); title('Diff�rence entre l''image et sa translat�e')

%% Suppression ringing bords

% On g�n�re une image de u sym�tris�e afin de calculer la 
% translation dessus. Cela permet de r�gulariser l'image
% p�riodique utilis�e pour le calcul de celle translat�e.
u_sym = fsym2(u);
figure(3); subplot(1,2,1); imshow(u, [0 255]); title('Image')
subplot(1,2,2); imshow(u_sym, [0 255]); title('Image sym�tris�e')

% On translate sur l'image sym�tris�e
v_sym = ffttrans(u_sym,0.5,0.5);
v_sym = v_sym(1:size(v, 1), 1:size(v, 2));

% On affiche l'image et sa nouvelle translat�e
figure(4); subplot(1,2,1); imshow(u,[0,255]); title('image originale')
subplot(1,2,2); imshow(v_sym,[0,255]); 
title('image d�cal�e de 1/2 avec sym�trisation')

figure(6); subplot(1,2,1); imshow(v,[0,255]); 
title('Translat�e sans sym�trisation')
subplot(1,2,2); imshow(v_sym,[0,255]); 
title('Translat�e avec sym�trisation')

% On affiche c�te � c�te les deux diff�rence
figure(5); subplot(1,2,1); imshow(u-v, []); 
title('Difference sans symetrisation')
subplot(1,2,2); imshow(v_sym - u, []); 
title('Difference avec symetrisation')
