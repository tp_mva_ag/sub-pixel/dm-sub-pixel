function [p,s] = perdecomp(u)
    [ny,nx] = size(u);  % On r�cup�re la taille de u
    u = double(u);      % On s'assure que u soit au format 'double'
    X = 1:nx; Y = 1:ny; % X et Y sont des vecteurs de coordon�es 
                    % pour parcourir u
    v = zeros(ny,nx);   % v matrice de 0 de taille de u

    % On g�n�re la matrice v, telle que d�finie dans le cours :
    % v(z)= sum_{y(\in)W(z)} [u(z) - u(y)]
    % Ainsi, v est non-nulle uniquement sur les bords, qui sont 
    % d�finis par la soustraction de la valeur de u sur ce bord, 
    % moins celle sur le bord 'oppos�'.
    v(1,X) = u(1,X)-u(ny,X);
    v(ny,X) = -v(1,X);
    v(Y,1 ) = v(Y,1 )+u(Y,1)-u(Y,nx);
    v(Y,nx) = v(Y,nx)-u(Y,1)+u(Y,nx);
    
    % On g�n�re ensuite deux matrices fx et fy, correspondant 
    % aux termes 2*cos(2*pi*q/M) et 2*cos(2*pi*r/N), qui seront 
    % utilis�s pour calculer terme � terme les valeurs de s
    fx = repmat(cos(2.*pi*(X -1)/nx),ny,1);
    fy = repmat(cos(2.*pi*(Y'-1)/ny),1,nx);
    fx(1,1)=0.;
    
    % Enfin, on g�n�re s � partir de la formule vu en cours, et 
    % on en d�duit p � partir de l'expression triviale p=u-s
    s = real(ifft2(fft2(v)*0.5./(2.-fx-fy)));
    p = u-s;

