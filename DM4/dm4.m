%% Exercice 11

u = double(imread('lena.pgm'));
[p,s] = perdecomp(u);

figure(1); subplot(1,3,1); imshow(u,[]); title('Image orignale')
subplot(1,3,2); imshow(p,[]); title('Composante p')
subplot(1,3,3); imshow(s,[]); title('Composante s')

disp(max(max(abs(u - p - s))))

%% Exercice 11.2

figure(2); 
subplot(1,2,1); imshow(kron(ones(2,2),u),[ ]); title('Image orignale')
subplot(1,2,2); imshow(kron(ones(2,2),p),[ ]); title('Composante p')

%% Exercice 11.3

figure(3)
fftU = fftshift(fft2(u)); fftP = fftshift(fft2(p));
subplot(1,2,1); imshow(log(fftU),[ ]); title('FFT de l''image orignale')
subplot(1,2,2); imshow(log(fftP),[ ]); title('FFT de la composante p')
