function [newU, newV] = exchange_phase(u, v, figureNb)
% Cette fonction �change la phase des deux images u et v

    if nargin<3
        figureNb = -1;
    end

    if (size(u) ~= size(v))
        disp("Images does not have the same size")
        newU = -1;
        newV = -1;
        return
    end

    % On calcule les FFT de u et v
    fftU = fft2(u); fftV = fft2(v);
    
    % On g�n�re les FFT des nouvelles images :
        % newU avec la norme de U et phase de V
        % newV avec la norme de V et phase de U
    fft_newU = abs(fftU) .* exp(1i * angle(fftV));
    fft_newV = abs(fftV) .* exp(1i * angle(fftU));

    % Enfin, on reforme des images � partir de ces nouvelles FFT
    newU = ifft2(fft_newU); newV = ifft2(fft_newV);

    if(figureNb >= 0)
        figure(figureNb);
        subplot(2,2,1); imshow(u, []); title('Image 1')
        subplot(2,2,2); imshow(v, []); title('Image 2')
        subplot(2,2,3); imshow(newU, []); title('Norme 1, Phase 2')
        subplot(2,2,4); imshow(newV, []); title('Norme 2, Phase 1')
    end
end

