function [newU, newV] = exchange_phase_file(fileU, fileV, figureNb)
    % Cette fonction �change la phase des deux images contenues dans les 
    % fichiers sp�cifi�s 
    
    if nargin<3
        figureNb = -1;
    end
        
    u = double(imread(fileU));
    v = double(imread(fileV));
    [newU, newV] = exchange_phase(u,v,figureNb);

end
