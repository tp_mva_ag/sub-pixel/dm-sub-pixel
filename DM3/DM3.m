%% Exercice 7.1

u = double(imread('lena.pgm')); % On charge l'image Lena
f = fft2(u);    % On en calcule la FFT

figure(70); imshow(u, []); title('l''image originale');
figure(71); imshow(fftshift(f)); title('FFT complexe de l''image')
figure(72); imshow(fftshift(abs(f))); title('norme de la FFT')
figure(73); imshow(fftshift(abs(f)),[]);  
title('norme de la FFT avec niveaux de gris automatiques')

% On applique la fonction normsat, qui �limine les saturations de la
% FFT afin d'am�liorer l'affichage
figure(75); imshow(normsat(fftshift(abs(f)),1));
title('norme de la FFT avec �limination des saturations avec normsat')

%% Question 7.2

figure(76); imshow(normsat(fftshift(real(f)),1));
title('Partie r�elle de la FFT')
figure(77); imshow(normsat(fftshift(imag(f)),1));
title('Partie imaginaire de la FFT')
figure(78); imshow(normsat(fftshift(angle(f)),1));
title('Argument de la FFT')
figure(79); imshow(normsat(fftshift(log(abs(f))),1));
title('Norme en �chelle logarithmique de la FFT')

%% Exercice 8.3

u = double(imread('lena.pgm')); figure(80);
subplot(1, 2, 1); imshow(u,[]); title('l''image originale')
v = fshift(u,-30,-30);
subplot(1, 2, 2); imshow(v,[]); title('l''image d�cal�e p�riodiquement')

fftU = fftshift(fft2(u));
fftV = fftshift(fft2(v));

figure(81)
subplot(1, 2, 1); imshow(log(abs(fftU)), ...
    [min(min(log(abs(fftU)))) max(max(log(abs(fftU))))]); 
title('Norme (log) de la FFT de l''image originale')
subplot(1, 2, 2); imshow(log(abs(fftV)),[min(min(log(abs(fftU)))) max(max(log(abs(fftU))))]); 
title('Norme (log) de la FFT de l''image d�cal�e')

figure(82); 
subplot(1, 2, 1); imshow(angle(fftU),[]); 
title('Argument de la FFT de l''image originale')
subplot(1, 2, 2); imshow(angle(fftV),[]);
title('Argument de la FFT de l''image d�cal�e')

figure(83); 
subplot(1, 2, 1); 
imshow(log(abs(fftU) - abs(fftV)), ...
    [min(min(log(abs(fftU)))) max(max(log(abs(fftU))))]); 
title('Norme de la difference des FFT')
subplot(1, 2, 2); 
imshow(angle(fftU) - angle(fftV),[min(min(angle(fftU))) max(max(angle(fftU)))]);
title('Argument de la diff�rence des FFT')

disp('Moyenne de la FFT originale'); disp(mean(mean(abs(fftU))));
disp('Moyenne de la FFT d�cal�e'); disp(mean(mean(abs(fftV))))
disp('Moyenne de la diff�rence des deux FFT'); 
disp(mean(mean(abs(fftU) - abs(fftV))))
% On trouve une diff�rence d'un facteur 10^17 ! Les deux normes sont les
% m�mes.

disp('Argument moyen de la FFT originale'); disp(mean(mean(angle(fftU))));
disp('Argument moyen de la FFT d�cal�e'); disp(mean(mean(angle(fftV))))
disp('Moyenne de la diff�rence des arguments des FFT'); 
disp(mean(mean(angle(fftU) - angle(fftV))))
% La valeur max de la diff�rence est du m�me ordre de grandeur que le max
% des arguments. Donc les arguments sont totalement diff�rents.

%% Exercice 10.1

exchange_phase_file('lena.pgm','room.pgm',100);
exchange_phase_file('lena.pgm','nimes.pgm',101);
exchange_phase_file('lena.pgm','saturne.pgm',102);
exchange_phase_file('room.pgm','saturne.pgm',103);

%% Exercice 10.2

u = double(imread('texture_sable.jpg'));
u = rgb2gray(u/255);
figure(110); imshow(u, [])

% p = perdecomp(u);
v = randphase(u);

figure(111);
imshow(v, []);

%% Exercice 10.3

nx = 512; I = -nx/2:-nx/2+nx-1; [X,Y] = meshgrid(I,I);
R = sqrt(X.^2+Y.^2);

u = (R<30);
figure(120); imshow(u); figure(121); imshow(randphase(u),[]);

% Rectangle
u = (abs(X)<30 & abs(Y)<40);
figure(122); subplot(2,3,1); imshow(u); title('Image originale')
figure(122); subplot(2,3,4); imshow(randphase(u),[]);
title('Texture g�n�r�e')
%Quadrillage
u = (mod(X,40) < 20 | mod(Y,40) < 20);
figure(122); subplot(2,3,2); imshow(u); title('Image originale')
figure(122); subplot(2,3,5); imshow(randphase(u),[]);
title('Texture g�n�r�e')
% Bandes
u = (mod(X,40) < 20);
figure(122); subplot(2,3,3); imshow(u); title('Image originale')
figure(122); subplot(2,3,6); imshow(randphase(u),[]);
title('Texture g�n�r�e')

%% Exercice 10.3.2

u = double(imread('texture.pgm'));
fftU = fftshift(fft2(u)); figure(130); 
subplot(1,2,1); imshow(u, [])
subplot(1,2,2); imshow(log(fftU), [])

m = 17.5;
theta = 15; a = 2*m; b = m; % Tests de param�tres
nx = size(u,2); Ix = -nx/2:-nx/2+nx-1;
ny = size(u,1); Iy = -ny/2:-ny/2+ny-1;
[X,Y] = meshgrid(Ix,Iy);

v = ((X.^2)/(a^2) + (Y.^2)/(b^2) < 1); 
v = imrotate(v, theta, 'nearest', 'crop');
randV = randphase(v);
figure(140); subplot(2,2,1);imshow(v);
title('Ellipse g�n�r�e')
subplot(2,2,2); imshow(randV,[]); title('Texture g�n�r�e')
subplot(2,2,3); imshow(log(fftshift(fft2(randV))), [])
title('FFT de la texture g�n�r�e')
subplot(2,2,4); imshow(log(fftU), []); 
title('FFT de la texture originale')

