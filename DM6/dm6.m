u = double(imread('crop_bouc.pgm'));
v = double(imread('crop_cameraman.pgm'));

figure(3); subplot(1,2,1); imshow(u, []); title('Image originale 1')
subplot(1,2,2); imshow(v, []); title('Image originale 2')

list_n = [0 1 5 -3];
num_n = numel(list_n);
nb_lignes = ceil(num_n/2);

for i=1:num_n
    n = list_n(i);
    u_z = fzoom(u,16,n);
    v_z = fzoom(v,16,n);
    
    figure(1); subplot(nb_lignes, 2, i);
    imshow(u_z, []); title(int2str(n));
    
    figure(2); subplot(nb_lignes, 2, i);
    imshow(v_z, []); title(int2str(n));
end


