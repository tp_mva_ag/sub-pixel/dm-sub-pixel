%% Question 4.1

% On ouvre et affiche l'image
u = double(imread('room.pgm'))/255;  % On ouvre l'image
figure(1); imshow(u);  % On l'affiche

% On va la sous-�chantillonner : on la passe de 512x512 � 512/3 x 512/3
lambda = 3; % Le "pas d'�chantillonnage"
v = u(1:lambda:end,1:lambda:end);   % v prend une valeur sur 3 de u
w = kron(v,ones(lambda));   % w prend les valeurs de v et les recopie pour faire les m�mes dimensions de u
[ny,nx] = size(u); 
figure(2); imshow([u,w(1:ny,1:nx)]);    % On affiche u et w

%% Question 4.3.a

% On cr�e une sinuso�de pure � partir d'un spectre avec un Dirac
f = zeros(512);     % On g�n�re une matrice vide
f(190,50) = 2;      % on met une valeur � 2, pour simuler un 'Dirac'
onde = real(ifft2(f));  % On calcule la TF inverse, puisque notre 
% spectre ne poss�de qu'une seule valeure ponctuelle, elle agira 
% comme Dirac et cr�era une forme p�riodique

figure(3); imshow(onde,[]); % On affiche l'image

% On recalcule la TF et on l'affiche
fourier = fftshift(abs(fft2(onde)));
figure(4); imshow(fourier);axis on

%% Question 4.3.b

% Comme pr�c�demment, on sous-�chantillonne l'image
lambda = 2; % Le "pas d'�chantillonnage"
v2 = onde(1:lambda:end,1:lambda:end);   % v prend une valeur sur 2 de u
w2 = kron(v2,ones(lambda)); 
[ny,nx] = size(onde); w2 = w2(1:ny,1:nx);

figure(5); imshow([onde,w2], []);    % On affiche u et w

% On recalcule la TF et on l'affiche
fourier = fftshift(abs(fft2(w2)));
figure(6); imshow(fourier);axis on

%% Execrice 5 : Question 5.1

% On observe l'effet de la mise au carr� de notre sinuso�de pure
f = zeros(512); f(190,50) = 2; onde1 = real(ifft2(f));
figure(10); imshow(onde1,[]);

% On la met au carr� et on l'affiche
onde2 = onde1.^2;
figure(11); imshow(onde2,[]);

% On calcule les FFT des deux images : simples et carr�
fourier1 = fftshift(abs(fft2(onde1))); 
fourier2 = fftshift(abs(fft2(onde2)));
figure(12); imshow(fourier1, []);axis on
figure(13); imshow(fourier2, []);axis on

%% Question 5.2

% On recalcule notre image, mais cette fois on utilise fftZoom pour
% agrandir le domaine de l'image et donc de la FFT
f = zeros(512); f(190,50) = 2; onde1 = real(ifft2(f));
ondeZoom1 = fftzoom(onde1, 2);
ondeZoom2 = ondeZoom1.^2;

figure(20); imshow(ondeZoom1,[]);
figure(21); imshow(ondeZoom2,[]);

% On calcule les FFT et on les affiche
fourierZoom1 = fftshift(abs(fft2(ondeZoom1)));
fourierZoom2 = fftshift(abs(fft2(ondeZoom2)));

figure(22); imshow(fourierZoom1, [0, max(max(fourierZoom1))/2]) ;axis on
figure(23); imshow(fourierZoom2, [0, max(max(fourierZoom2))/10]);axis on

%% Question 5.3

% On prend une image et on en calcule le gradient discret
nimes = double(imread('nimes.pgm'))/255;  % On ouvre l'image
figure(30); imshow(nimes(200:250, 250:300));  % On l'affiche

% Gradient Discret
v = gradn(nimes);
figure(31); imshow(v(200:250, 250:300), []);

%% Question 5.3.b

% Pour corriger les erreurs du gradient, on applique la technique
% pr�c�dente du zoom
nimesZoom = fftzoom(nimes, 2);
figure(32); imshow(nimesZoom(400:500, 500:600));  % On l'affiche

vZoom = gradn(nimesZoom);
figure(33); imshow(vZoom(400:500, 500:600), []);


%% La fonction gradn(u)

function v = gradn(u)
% prend en entr�e une image u de taille (M,N) et 
% renvoie une image v de taille (M?1, N?1), 
% norme du gradient discret de u

[ny,nx] = size(u);

v = sqrt(...
            (u(1:ny-1,2:nx) - u(1:ny-1,1:nx-1)).^2 + ...
            (u(2:ny,1:nx-1) - u(1:ny-1,1:nx-1)).^2 );
end
